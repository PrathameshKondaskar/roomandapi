package com.example.demoairit.Retrofit;

import com.example.demoairit.models.Contacts;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitService {

    @GET("/json/contacts.json")
    Call<List<Contacts>> getContacts();

}
