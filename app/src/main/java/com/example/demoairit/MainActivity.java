package com.example.demoairit;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.demoairit.Retrofit.RetrofitInstance;
import com.example.demoairit.Retrofit.RetrofitService;
import com.example.demoairit.RoomDatabase.AppDatabase;
import com.example.demoairit.adapters.CustomAdapter;
import com.example.demoairit.models.Contacts;

import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    //Views
    private RecyclerView recyclerView;
    private ProgressBar progressBar;


    private List<Contacts> contactsList;
    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = Room.databaseBuilder(this, AppDatabase.class, AppDatabase.DATABASE_NAME).build();

        try {
            contactsList = new RoomGetAsync().execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (contactsList.size() > 0) {

            parseData(contactsList);
        } else {
            RetrofitService retrofitService = RetrofitInstance.getRetrofitInstance().create(RetrofitService.class);
            Call<List<Contacts>> call = retrofitService.getContacts();

            call.enqueue(new Callback<List<Contacts>>() {
                @Override
                public void onResponse(Call<List<Contacts>> call, Response<List<Contacts>> response) {
                    if (response.isSuccessful()) {

                        parseData(response.body());
                    }
                }

                @Override
                public void onFailure(Call<List<Contacts>> call, Throwable t) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("No internet Connection");
                    builder.setMessage("Please turn on internet connection to continue");
                    builder.setNegativeButton("close", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }
            });

        }
    }

    private void parseData(List<Contacts> body) {
        progressBar.setVisibility(View.GONE);
        CustomAdapter customAdapter = new CustomAdapter(this, body);
        new RoomInsertAsync(body).execute();


        recyclerView.setAdapter(customAdapter);
    }

    public class RoomInsertAsync extends AsyncTask<Void, Void, Void> {

        private List<Contacts> contactsList;

        public RoomInsertAsync(List<Contacts> contactsList) {
            this.contactsList = contactsList;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            db.contactsDao().insertContacts(contactsList);
            Log.d("data", "inserted");
            return null;
        }
    }

    public class RoomGetAsync extends AsyncTask<Void, Void, List<Contacts>> {


        @Override
        protected List<Contacts> doInBackground(Void... voids) {
            return db.contactsDao().getAllContacts();
        }
    }


}
