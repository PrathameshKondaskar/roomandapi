package com.example.demoairit.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "contacts")
public class Contacts {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo
    public String name ;

    @ColumnInfo
    public String image ;

    @ColumnInfo
    public String phone ;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] imageByte;

    public Contacts(int id, String name, String phone, byte[] imageByte) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.imageByte = imageByte;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public byte[] getImageByte() {
        return imageByte;
    }

    public void setImageByte(byte[] imageByte) {
        this.imageByte = imageByte;
    }
}
