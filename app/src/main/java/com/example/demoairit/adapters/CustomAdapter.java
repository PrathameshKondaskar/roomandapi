package com.example.demoairit.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import androidx.room.Room;

import com.example.demoairit.R;
import com.example.demoairit.RoomDatabase.AppDatabase;
import com.example.demoairit.models.Contacts;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.RecyclerHolder> {


    Context context;
    List<Contacts> contactsList;
    AppDatabase db;

    public CustomAdapter(Context context, List<Contacts> models) {
        this.context = context;
        this.contactsList = models;
        db = Room.databaseBuilder(context, AppDatabase.class, AppDatabase.DATABASE_NAME).build();
    }

    @NonNull
    @Override
    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.custom_list, null, false);


        return new RecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {

        holder.tvName.setText(contactsList.get(position).name);
        holder.tvPhone.setText(contactsList.get(position).phone);

        Picasso.Builder builder = new Picasso.Builder(context);

        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(contactsList.get(position).image).into(holder.imageView);

        holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (contactsList.size() > 0) {
                    new RoomDeleteAsync(contactsList.get(position)).execute();
                    contactsList.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, contactsList.size());
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

    public class RecyclerHolder extends ViewHolder {

        TextView tvName, tvPhone;
        ImageView imageView;
        Button buttonDelete;

        public RecyclerHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvPhone = itemView.findViewById(R.id.tvPhone);
            imageView = itemView.findViewById(R.id.imageView);
            buttonDelete = itemView.findViewById(R.id.bottonDelete);
        }
    }


    public class RoomDeleteAsync extends AsyncTask<Void, Void, Void> {

        private Contacts contacts;

        public RoomDeleteAsync(Contacts contacts) {
            this.contacts = contacts;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            db.contactsDao().delete(contacts);
            Log.d("data", "deleted");
            return null;
        }
    }


}
