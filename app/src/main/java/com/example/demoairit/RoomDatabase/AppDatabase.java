package com.example.demoairit.RoomDatabase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.demoairit.models.Contacts;

@Database(entities = {Contacts.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {


    public final static String DATABASE_NAME = "contacts_db";

    public abstract ContactsDao contactsDao();

}
