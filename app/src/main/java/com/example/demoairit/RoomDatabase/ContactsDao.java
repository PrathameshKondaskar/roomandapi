package com.example.demoairit.RoomDatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.demoairit.models.Contacts;

import java.util.List;

@Dao
public interface ContactsDao {

    @Query("SELECT * FROM contacts")
    List<Contacts> getAllContacts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertContacts(List<Contacts> contactsList);


    @Delete
    void delete(Contacts contacts);


}
